
name := "PubNativeChallenge"

version := "0.1"

scalaVersion := "2.12.12"

val sparkVersion = "3.0.0"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

libraryDependencies ++= Seq(
  "io.spray"         %%  "spray-json"  % "1.3.5",
  "joda-time"        %   "joda-time"   % "2.10.6",
  "com.typesafe.play" %% "play-json" % "2.9.0",
  "org.apache.spark" %% "spark-core" % sparkVersion,// % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion,// % "provided"
)
