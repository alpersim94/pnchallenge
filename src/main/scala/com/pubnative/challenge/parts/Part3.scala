package com.pubnative.challenge.parts

import com.pubnative.challenge.models.{Click, TopAdvertisers}
import com.pubnative.challenge.util.FileUtils.{readArgsAndReturnFiles, writeTopAdvertisersToJsonFile}
import com.pubnative.challenge.util.JsonParsers.{parseClicks, parseImpressions}
import org.apache.spark.sql.SparkSession

object Part3 {

  case class ImpressionCount(countryCode: String,
                             appId: Int,
                             impressionId: String,
                             impressionCount: Int,
                             advertisers: Int)

  case class ImpressionClick(impressionId: String,
                             countryCode: String,
                             appId: Int,
                             impressionCount: Int,
                             advertisers: Int,
                             revenue: Double)

  case class AdvertiserRate(appId: Int, countryCode: String, rate: Double, advertiserId: Int)

  val spark: SparkSession = SparkSession
    .builder()
//    .master("local[*]")
    .appName("PNChallenge")
    .getOrCreate()

  def partThree(args: Array[String]): Unit = {
    import spark.implicits._

    val (clickJson, impressionJson) = readArgsAndReturnFiles(args)

    // parse clicks jsons
    val clicks = parseClicks(clickJson)
    // parse impressions jsons
    val impressions = parseImpressions(impressionJson)

    val clicksDS = clicks.toDS()

    // Filter the null country codes and ids from impressions
    val impressionsDS = impressions.toDS()
      .filter(a => a.id != null && a.id != "" && a.countryCode != "null")

    // group clicks by impression id find total revenue per click
    // because we will measure the performance by revenue / impressions
    val groupedClicks = clicksDS.groupByKey(_.impressionId).mapGroups {
      (key, it) =>
        Click(key, it.map(_.revenue).sum)
    }

    // group impressions by country code, app id and impression id
    val groupedImpressions = impressionsDS.groupByKey(a => (a.countryCode, a.appId, a.id))
      .mapGroups {
        case ((countryCode, appId, impressionId), it) =>
          val listOfValues = it.toList
          ImpressionCount(countryCode, appId, impressionId,
            listOfValues.length,
            listOfValues.map(_.advertiserId).distinct.head)
      }.filter(a => a.countryCode.nonEmpty && a.countryCode != null)

    // join impressions and clicks
    val joinedImpressionsClicks = groupedImpressions
      .join(groupedClicks, Seq("impressionId"), "left")
      .na.drop
      .as[ImpressionClick]

    // find advertiser rates per country and app id
    val advertiserRatesDS = joinedImpressionsClicks
      .map {
        v =>
          val rate = v.revenue / v.impressionCount
          AdvertiserRate(v.appId, v.countryCode, rate, v.advertisers)
      }

    // best 5 advertisers for (country, appId)
    val groupedAppAndCountry = advertiserRatesDS.groupByKey {
      advRate =>
        (advRate.appId, advRate.countryCode)
    }.mapGroups {
      case ((appId, countryCode), advertisersIterator) =>
        val advertisers = advertisersIterator.toList.sortBy(_.rate).take(5).map(_.advertiserId)
        TopAdvertisers(appId, countryCode, advertisers)
    }.filter(_.app_id != -1)

    // write output to TopAdvertisers.json file in pretty format
    writeTopAdvertisersToJsonFile(groupedAppAndCountry.collect().toList, "TopAdvertisers.json")

  }
}
