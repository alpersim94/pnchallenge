package com.pubnative.challenge.parts

import com.pubnative.challenge.models.Impression
import com.pubnative.challenge.util.FileUtils.{readArgsAndReturnFiles, writeAppPerformancesToJsonFile}
import com.pubnative.challenge.util.Helpers.{groupByCountryAndAppId, joinImpressionsAndClicks, reduceClicks}
import com.pubnative.challenge.util.JsonParsers.{parseClicks, parseImpressions}

object Part2 {

  case class RevenueCount(revenue: Double, count: Int)

  def partTwo(args: Array[String]): Unit = {

    val (clickJson, impressionJson) = readArgsAndReturnFiles(args)

    // parse clicks jsons
    val clicks = parseClicks(clickJson)
    // parse impressions jsons
    val impressions = parseImpressions(impressionJson)

    // reduce clicks by impression id to find revenue sum and click count per impression
    val reducedClicks = reduceClicks(clicks)

    // map the reduced clicks to the Map
    val mappedClicks: Map[String, RevenueCount] = reducedClicks.map {
      click => (click.impressionId, RevenueCount(click.revenue, click.clickCount))
    }.toMap

    // group the impressions distinctly and find the total count per event
    val impressionCounts: Map[Impression, Int] = impressions.groupBy(identity).mapValues(_.size)

    // join impressions and clicks
    val joinedImpsAndClicks = joinImpressionsAndClicks(mappedClicks, impressionCounts)

    // find the performance of apps on the countries
    val output = groupByCountryAndAppId(joinedImpsAndClicks)

    // write the output to the output.json file under resources
    writeAppPerformancesToJsonFile(output, "AppPerformance.json")
  }
}
