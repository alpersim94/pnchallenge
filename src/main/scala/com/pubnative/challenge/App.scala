package com.pubnative.challenge

import com.pubnative.challenge.parts.{Part2, Part3}

object App {

  def main(args: Array[String]): Unit = {
    Part2.partTwo(args)
    Part3.partThree(args)
  }
}
