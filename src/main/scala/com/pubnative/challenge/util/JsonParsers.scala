package com.pubnative.challenge.util

import com.pubnative.challenge.models.{Click, Impression}
import play.api.libs.json.JsValue
import spray.json.JsObject
import scala.collection.immutable
import scala.util.Try
import spray.json
import spray.json._

object JsonParsers {

  /**
   * Parse list of fields from the json.
   */
  def parseDesiredFields(fieldName: List[String], record: String): immutable.Seq[json.JsValue] = {
    record.parseJson.asJsObject().getFields(fieldName: _*)
  }

  /**
   * parse one field from json
   */
  def parseFieldFromJson(fieldName: String)(implicit jsonObject: JsObject): String = {
    Try(jsonObject.getFields(fieldName).head.toString()).getOrElse("null")
  }

  /**
   * parse clicks from jsons and cast Click case class.
   */
  def parseClicks(clicksJson: List[JsValue]): List[Click] = {
    clicksJson.map {
      click =>
        implicit val impressionJsObject: JsObject = click.toString().parseJson.asJsObject()
        val impressionId = parseFieldFromJson("impression_id")
        val revenue = Try(parseFieldFromJson("revenue").toDouble).getOrElse(-1.0)
        Click(impressionId, revenue)
    }
  }

  /**
   * parse impressions from jsons and cast Impression case class.
   */
  def parseImpressions(impressionsJson: List[JsValue]): List[Impression] = {
    impressionsJson.map {
      impression =>
        implicit val impressionJsObject: JsObject = impression.toString().parseJson.asJsObject()
        val appId = Try(parseFieldFromJson("app_id").toInt).getOrElse(-1)
        val advertiserId = Try(parseFieldFromJson("advertiser_id").toInt).getOrElse(-1)
        val countryId = parseFieldFromJson("country_code").replace("\"", "")
        val id = parseFieldFromJson("id")
        Impression(appId, advertiserId, countryId, id)
    }
  }

}
