package com.pubnative.challenge.util


import com.pubnative.challenge.parts.Part2.RevenueCount
import com.pubnative.challenge.models.{Click, ClickCount, Impression, ImpressionRevenue, AppPerformance}
import scala.collection.immutable
import scala.util.Try

object Helpers {

  /**
   * For part 2 reduce the clicks and find the revenue sum.
   */
  def reduceClicks(clicks: List[Click]): immutable.Iterable[ClickCount] = {
    clicks.groupBy(_.impressionId).mapValues {
      clickList =>
        (clickList.map(_.revenue).sum, clickList.size)
    }.map { case (id, (revenue, clickCount)) => ClickCount(id, revenue, clickCount) }
  }


  /**
   * For part 2 join impressions and clicks.
   */
  def joinImpressionsAndClicks(clickMap: Map[String, RevenueCount], impressionCounts: Map[Impression, Int]):
  List[ImpressionRevenue] = {
    impressionCounts.filter(_._1.id != "").map {
      case (imp, impressionCount) =>
        val revenue = Try(clickMap(imp.id).revenue).getOrElse(0.0)
        val clickCount = Try(clickMap(imp.id).count).getOrElse(0)
        ImpressionRevenue(imp.id, revenue, imp.appId, imp.countryCode, impressionCount, clickCount)
    }.toList
  }

  /**
   * For part 2 group by the country code and app id to find app performances.
   */
  def groupByCountryAndAppId(impressionRevenue: List[ImpressionRevenue]): List[AppPerformance] = {
    impressionRevenue.groupBy(ic => (ic.countryCode, ic.appId)).mapValues {
      values =>
        val revenue = values.map(_.revenue).sum
        val impressions = values.map(_.impressionCount).sum
        val clicks = values.map(_.clickCount).sum
        (revenue, impressions, clicks)
    }.map {
      case ((countryCode, appId), (revenue, impressions, clicks)) =>
        AppPerformance(appId, countryCode, impressions, clicks, revenue)
    }.filter(output => output.country_code != "null" && output.country_code.nonEmpty).toList
  }

}
