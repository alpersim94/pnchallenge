package com.pubnative.challenge.util

import java.io.{File, FileInputStream, PrintWriter}

import com.pubnative.challenge.models.{AppPerformance, TopAdvertisers}
import play.api.libs.json.{JsValue, Json}

object FileUtils {

  /**
   * Read multiple json files and create one list with all of the values.
   */
  def readFromMultipleFiles(fileNames: Seq[String]): List[JsValue] = {
    fileNames.flatMap(readJsonFromFile).toList
  }

  /**
   * Read json array from a file.
   */
  def readJsonFromFile(fileName: String): List[JsValue] = {
    val inputStream = new FileInputStream(fileName)
    try {
      Json.parse(inputStream).as[List[JsValue]]
    }
    finally {
      inputStream.close()
    }
  }

  /**
   * write the results of part 2 to the json file.
   */
  def writeAppPerformancesToJsonFile(output: List[AppPerformance], filename: String): Unit = {
    import com.pubnative.challenge.models.AppPerformanceJsonProtocol._
    import spray.json._

    val pw = new PrintWriter(new File(s"$filename" ))
    pw.write(output.toJson.prettyPrint)
    pw.close()
  }

  /**
   * write the results of part 3 to the json file.
   */
  def writeTopAdvertisersToJsonFile(output: List[TopAdvertisers], filename: String): Unit = {
    import com.pubnative.challenge.models.TopAdvertisersJsonProtocol._
    import spray.json._

    val pw = new PrintWriter(new File(s"$filename" ))
    pw.write(output.toJson.prettyPrint)
    pw.close()
  }

  /**
   * read file names from the program arguments and return them as List[JsValue]
   */
  def readArgsAndReturnFiles(args: Array[String]): (List[JsValue], List[JsValue]) = {
    var clickFiles = Seq("clicks.json")
    var impressionFiles = Seq("impressions.json")

    if(args.nonEmpty) {
      if (args(0) == "-c" && args.length > 1) clickFiles = args(1).split(",")
      if (args(0) == "-i" && args.length > 1) impressionFiles = args(1).split(",")
      if (args.length > 2) {
        if (args(2) == "-i") impressionFiles = args(3).split(",")
      }
      if (args(0) != "-c" && args(0) != "-i")
        println("for click files use \"-c <file>,<file>...\" \nfor impressions files use \"-i <file>,<file>...\"")
    }

    // read multiple files at once
    val clickJson = readFromMultipleFiles(clickFiles)
    val impressionJson = readFromMultipleFiles(impressionFiles)

    (clickJson, impressionJson)
  }

}
