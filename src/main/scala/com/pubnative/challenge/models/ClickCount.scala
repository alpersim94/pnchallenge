package com.pubnative.challenge.models

case class ClickCount (impressionId: String, revenue: Double, clickCount: Int)
