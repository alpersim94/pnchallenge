package com.pubnative.challenge.models

case class AppPerformance(app_id: Int,
                          country_code: String,
                          impressions: Int,
                          clicks: Int,
                          revenue: Double)

import spray.json.DefaultJsonProtocol

object AppPerformanceJsonProtocol extends DefaultJsonProtocol {
  implicit val appPerformanceFormat = jsonFormat5(AppPerformance.apply)
}

