package com.pubnative.challenge.models

case class Click(impressionId: String, revenue: Double)
