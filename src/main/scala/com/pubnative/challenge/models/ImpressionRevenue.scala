package com.pubnative.challenge.models

case class ImpressionRevenue(id: String,
                             revenue: Double,
                             appId: Int,
                             countryCode: String,
                             impressionCount: Int,
                             clickCount: Int)
