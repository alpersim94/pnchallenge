package com.pubnative.challenge.models

case class TopAdvertisers(app_id: Int,
                          country_code: String,
                          recommended_advertiser_ids: List[Int])

import spray.json.DefaultJsonProtocol

object TopAdvertisersJsonProtocol extends DefaultJsonProtocol {
  implicit val topAdvertisersFormat = jsonFormat3(TopAdvertisers.apply)
}
