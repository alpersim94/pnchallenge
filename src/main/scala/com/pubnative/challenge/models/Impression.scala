package com.pubnative.challenge.models

case class Impression(appId: Int, advertiserId: Int,
                      countryCode: String, id: String)
