# Challenge

## Requirements
* Scala 2.12.x
* Sbt 
* Apache Spark 3.0 

## How to Run
* For part 2 and 3 I created separate files. 
    * Part 2 is pure Scala code
    * Part 3 is written with Apache Spark
    
* Each part uses part 1 to read the json files. 
    * Normally I prefer spray-json as a json parser and I used it in here to parse jsons but 
    to be able to read json arrays like in the test files I used play frameworks Json 
    library. 
    
* To be able to run it there are two options. 
   
Under the project folder 

Assemble the jar with Spark dependencies
```
sbt assembly
``` 
and use the command down below with 
either giving filenames separated with coma for click "-c \<file>,\<file>" 
for impressions "-i \<file>,\<file>" or don't give the filenames to use test files. 
```
java -jar target/scala-2.12/PubNativeChallenge-assembly-0.1.jar -c clicks.json -i impressions.json
```

Or use spark-submit to run. If the application will run on Yarn or Mesos etc. don't forget to change the master.
```
spark-submit target/scala-2.12/PubNativeChallenge-assembly-0.1.jar -c clicks.json -i impressions.json --master "local[*]" --num-executors 4 --executor-cores 2 --executor-memory 1G --class com.pubnative.challenge.App```
```

Because the instance has 8 cores I decided to go with 4 executors and 2 core per executor.

For part 2 the output file is AppPerformance.json

For part 3 the output file is TopAdvertisers.json

